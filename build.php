<?php

$pages = json_decode('{"pages":[' . file_get_contents('pages') . ']}')->pages;

foreach ($pages as $key => $value) {
    if (file_exists("$value.php")) {
        ob_start();
        include("$value.php");
        $content = str_replace('.php', '.html', ob_get_clean());
        file_put_contents("$value.html", $content);
        echo "$value.html -> converted <br/>";
    }
}

echo "Convert finish";

if (!isset($_SERVER['REMOTE_ADDR'])) {
    file_put_contents(".gitignore", "
*.php
.gitlab-ci.yml
includes/
pages
*.scss
");
}
